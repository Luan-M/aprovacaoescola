package domain;

import java.util.Scanner;

public class AprovacaoEscola {
	
	static final Integer NOTA_MINIMA_PORTUGUES = 70;
	static final Integer NOTA_MINIMA_MATEMATICA = 70;
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Digite o nome do(a) aluno(a): ");
		String nome = scanner.next();
		
		System.out.print("Digite a nota em português: ");
		Integer notaPortugues = scanner.nextInt();
		
		System.out.print("Digite a nota em matemática: ");
		Integer notaMatematica = scanner.nextInt();
		
		Boolean aprovadoPortugue = notaPortugues >= NOTA_MINIMA_PORTUGUES;
		Boolean aprovadoMatematica = notaMatematica >= NOTA_MINIMA_MATEMATICA;
		
		Boolean notaAtingida = aprovadoPortugue && aprovadoMatematica;
		
		if(notaAtingida) {
			System.out.println("O(A) aluno(a) " + nome + " foi aprovado.");
		} else {
			System.out.println("O(A) aluno(a) " + nome + " não foi aprovado.");
		}
		
		scanner.close();
	}

}
